import React from "react";
import "./Home.css";
import Typewriter from "typewriter-effect";
import MyCv from "../../Image/user.png";
const Home = () => {
  return (
    <div className="container-fluid home">
      <div className="container home-content">
        <h1>Hi I'm a</h1>
        <h3>
          <Typewriter
            options={{
              strings: [
                "Full stack Developer",
                "Android Developer",
                "Web Developer",
                "DBA",
                "Technical Lead",
              ],
              autoStart: true,
              loop: true,
              delay: 5,
            }}
          />
        </h3>
        <div className="button-for-action">
          <div className="hire-me-button"> Hire me</div>
          <div className="resume-button">
            <a href={MyCv} download="AKSHAYVAIDYA">
              Resume
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
