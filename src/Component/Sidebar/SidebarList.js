import React from "react";
import "./SidebarList.css";
import Image from "../../Image/user.png";
import {
  FcHome,
  FcNightPortrait,
  FcFactory,
  FcTodoList,
  FcContacts,
  FcSalesPerformance,
} from "react-icons/fc";
import { MdBiotech, MdCastForEducation } from "react-icons/md";
const SidebarList = ({ expand }) => {
  return (
    <React.Fragment>
      {expand ? (
        <>
          <div className="navbar-items">
            <div className="sidebar-profile-pic">
              <img src={Image} alt="akshay"></img>
            </div>

            <ul>
              <li className="nav-item">
                <FcHome size={25}></FcHome>Home
              </li>
              <li className="nav-item">
                <FcNightPortrait size={25}></FcNightPortrait>About
              </li>
              <li className="nav-item">
                <FcFactory size={25}></FcFactory>Work Experience
              </li>
              <li className="nav-item">
                <MdBiotech size={25} color="orange"></MdBiotech>Tech Stack
              </li>
              <li className="nav-item">
                <MdCastForEducation
                  color="yellow"
                  size={25}
                ></MdCastForEducation>
                Education
              </li>
              <li className="nav-item">
                <FcTodoList size={25}></FcTodoList>Projects
              </li>
              <li className="nav-item">
                <FcSalesPerformance size={25}></FcSalesPerformance>Testimonial
              </li>
              <li className="nav-item">
                <FcContacts size={25}></FcContacts>Contact
              </li>
            </ul>
          </div>
        </>
      ) : (
        <div className="nav-bar-item-only-icons">
          <ul>
            <li className="nav-item">
              <FcHome size={25}></FcHome>
            </li>
            <li className="nav-item">
              <FcNightPortrait size={25}></FcNightPortrait>
            </li>
            <li className="nav-item">
              <FcFactory size={25}></FcFactory>
            </li>
            <li className="nav-item">
              <MdBiotech size={25} color="orange"></MdBiotech>
            </li>
            <li className="nav-item">
              <MdCastForEducation size={25} color="yellow"></MdCastForEducation>
            </li>
            <li className="nav-item">
              <FcTodoList size={25}></FcTodoList>
            </li>
            <li className="nav-item">
              <FcSalesPerformance size={25}></FcSalesPerformance>
            </li>
            <li className="nav-item">
              <FcContacts size={25}></FcContacts>
            </li>
          </ul>
        </div>
      )}
    </React.Fragment>
  );
};

export default SidebarList;
