import React, { useState } from "react";

import "./Sidebar.css";
import { BsChevronBarLeft, BsChevronBarRight } from "react-icons/bs";
import Home from "../Home/Home";
import SidebarList from "./SidebarList";
const Sidebar = () => {
  const [expand, setExpand] = useState(true);

  const handleExpandClick = () => {
    setExpand(!expand);
  };
  return (
    <div className="container-fluid sidebar-section">
      <div className={expand ? "sidebar-expand sidebar" : "sidebar"}>
        <div className="icon-for-sidebar-expand-and-collapse">
          <p onClick={handleExpandClick}>
            {expand ? (
              <BsChevronBarLeft size={30}></BsChevronBarLeft>
            ) : (
              <BsChevronBarRight size={30}></BsChevronBarRight>
            )}
          </p>
        </div>
        <SidebarList expand={expand}></SidebarList>
      </div>
      <div
        style={expand ? { marginLeft: "230px" } : { marginLeft: "95px" }}
        className="container"
      >
        <Home></Home>
      </div>
    </div>
  );
};

export default Sidebar;
